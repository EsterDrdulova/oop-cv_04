﻿namespace cv4;
class Program
{
    static void Main(string[] args)
    {
        string TextForTesting = "Toto je retezec predstavovany nekolika radky,\n"
        + "ktere jsou od sebe oddeleny znakem LF (Line Feed).\n"
        + "Je tu i nejaky ten vykricnik! Pro ucely testovani i otaznik?\n"
        + "Toto je jen zkratka zkr. ale ne konec vety. A toto je\n"
        + "posledni veta!";

        StringStatistics text = new StringStatistics(TextForTesting);

        Console.WriteLine("Number of words: " + text.CountWords());
        Console.WriteLine("Number of lines: " + text.CountLines());
        Console.WriteLine("Number of sentenses: " + text.CountSentenses());
        Console.WriteLine("Longest words: " + string.Join(",", text.LongestWords()));
        Console.WriteLine("Shortest words: " + string.Join(",", text.ShortestWords()));
        Console.WriteLine("Most common word: " + string.Join(",", text.MostCommonWord()));
        Console.WriteLine("Alphabeticaly sorted: " + string.Join(",", text.AbecedlySorted()));


    }
}

