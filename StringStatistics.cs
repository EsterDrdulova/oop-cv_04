﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace cv4
{
	public class StringStatistics
	{
		string text;
		string trimedText;
        string[] allWords;
        List<string> orderedWords;

        public StringStatistics(String inputedText)
		{
			this.text = inputedText;
            trimedText = text.Replace(".", "").Replace(",", "").Replace("?", "").Replace("!", "")
                .Replace("\n", " ").Replace("(", "").Replace(")", "");
            allWords = trimedText.Split(' ');
            orderedWords = allWords.OrderBy(x => x.Length).ToList();

        }

		public int CountWords()
		{
            return allWords.Length;
		}

		public int CountLines()
		{
            string[] numberOfLines = Regex.Split(text, @"\n");
            return numberOfLines.Length;
		}

        public int CountSentenses()
        {
            string[] splitSentences = Regex.Split(text, @"[\.!\?]+\s(?=[A-Z])"); 
            return splitSentences.Length;

        }

        public string[] MostCommonWord()
        {
            var sorted = allWords.GroupBy(x => x);
            var maxCount = sorted.Max(g => g.Count());
            return sorted.Where(x => x.Count() == maxCount).Select(x => x.Key).ToArray();
        }

        public string[] LongestWords()
        {
            orderedWords.Reverse();
            var listOfLongestWords = new List<string>();
            var len = 0;
            foreach (var word in orderedWords)
            {
                if(word.Length >= len)
                {
                    len = word.Length;
                    listOfLongestWords.Add(word);
                }
            }
            return listOfLongestWords.ToArray();

        }

        public string[] ShortestWords()
        {
            orderedWords.Reverse();
            var listOfShortestWords = new List<string>();
            var len = allWords.Length;
            foreach (var word in orderedWords)
            {
                if (word.Length <= len)
                {
                    len = word.Length;
                    listOfShortestWords.Add(word);
                }
            }

            return listOfShortestWords.ToArray();
        }

        public string[] AbecedlySorted()
        {
           Array.Sort(allWords);
           return allWords;
        }

        

    }
}

